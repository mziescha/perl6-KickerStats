use v6.c;
use Bailador;
use Bailador::RouteHelper;
use Bailador::Plugin::NamedQueries;

use KickerStats::DB;
use KickerStats::User;

my $version = '0.0.1';


class KickerStats is Bailador::App {

    sub _html_render_fix(Hash:D $hash){{html =>$hash}}

    submethod BUILD(|) {
        self.add_route: make-route('GET','/' => sub {
            return self.redirect( '/insall') if !KickerStats::DB.new( named_queries => self.plugins.get('NamedQueries').object ).check;
            template 'index.html', _html_render_fix {version => $version,}
        });

        self.add_route: make-route('GET','/install' => sub {
            template 'index.html', _html_render_fix { 
                  version => $version,
                  result => self.plugins.get('NamedQueries').write: 'user/create',
            }
        });

        self.add_route: make-route('GET','/users' => sub {
            template 'index.html', _html_render_fix {
                version => $version, 
                result  => self.plugins.get('NamedQueries').read: 'user/select'}
        });

        self.add_route: make-route('GET','/developers' => sub {
            my $user = KickerStats::User.new( named_queries => self.plugins.get('NamedQueries').object );
            template 'index.html', _html_render_fix {
                version => $version, 
                result  => $user.list_by_description('developer'),
            }
        });

        self.add_route: make-route('POST','/insert/:description' => sub ( $description ) {
            my $user = KickerStats::User.new( named_queries => self.plugins.get('NamedQueries').object );
            $user.insert({ name => self.request.params<name>, description => $description.lc, });
            template 'index.html', _html_render_fix {
                version => $version, 
                test => $description.Str,
            }
        });

        self.add_route: make-route('GET','/from' => sub {self.redirect: '/to' });
        self.add_route: make-route('GET','/to' => sub { 'Arrived to.' });
    }
}
