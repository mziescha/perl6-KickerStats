use v6.c;

use DBIx::NamedQueries;

my $version = '0.0.1';

class KickerStats::DB {
    
    has DBIx::NamedQueries $.named_queries is required;

    method check(){
        my $result = $!named_queries.list('Schema', { type => 'table' });
        return $result.elems > 0 ?? True !! False;
    }

    method init(Str:D $description){
        #$!named_queries.write('user/insert',{
        #    name => $parms{'name'},
        #    description => $parms{'description'},
        #    quantity => 1e1.rand,
        #    price => 1e3.rand,
        #});        
    }
}
