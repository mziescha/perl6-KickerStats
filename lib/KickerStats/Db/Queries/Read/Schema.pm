use DBIx::NamedQueries;

class KickerStats::Db::Queries::Read::Schema does DBIx::NamedQueries::Read {

    method select( %params ) { 
        return {
            statement => q@
                
            @,
        };
    }
    
    method list( %params ) {
        return {
            fields => [
                {
                    name => 'type'
                },
            ],
            statement => q@
                SELECT
                    name
                FROM
                    sqlite_master
                WHERE 1
            @ ~ ( %params<type>:exists ?? q@AND type = ?@ !! q@@ )
            ,
        };
    }
    method find( %params ) {  }
}
