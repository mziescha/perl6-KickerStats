# for DBI::Async

```
libtool libtommath-dev libatomic-ops-dev libuv1-dev libffi-dev
```
### Perl6 packages

```
zef install JSON::Name
zef install JSON::Marshal
zef install JSON::Unmarshal
zef install JSON::Class
zef install META6
zef install HTTP::HPACK
zef install HTTP::Request::Supply
zef install Bailador
zef install DBI::Async
zef install Data::Dump
```
